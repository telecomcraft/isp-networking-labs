# ISP Networking Labs

These network labs are designed to focus specifically on configuring and operating service provider networks. When I got into the industry, such content was very difficult to find and generally either private/proprietary or outdated. My goal is to develop a library of lab activities that utilize opensource technologies to learn how to build and operate an "open ISP."

## Overview

The labs will proceed through the general steps of setting up an ISP, strictly from the inside plant perspective (so no utility poles, conduit, and other outside plant considerations). This involves IP address planning, DHCP, routing, firewalling, DNS, QoS, and other core network functions.

From there, we will create and provision services for subscribers, and build out the subscriber sides of the network as well. Here we'll look at WAN configurations, security, redundancy, and LAN-side topics. I believe this is important to help understand how the two sides of the network interact and consider all the levels of connectivity that exist to delivery quality and secure services.

### Technology

The labs will be performed using opensource tools in a virtualized environment. This includes:

- The Virtualbox Hypervisor, an easy-to-use platform to run virtual machines and networks
- VyOS, an excellent network operating system (NOS) that has nearly all of the network functions needed to operate a network
- Ubuntu Server and Ubuntu Desktop, one of the most popular Linux distributions
- ISC BIND9 for advanced DNS servers
- ISC Kea for advanced DHCP servers
- Zabbix for network monitoring
- NetBox for IPAM and DCIM recordkeeping
- PostgreSQL, the most powerful RDBMS, for a data store (Kea, Zabbix, NetBox, etc.)
- Python, for automation

### Topology

The lab network topology will consist of two sides when fully operational: two ISP networks and two subscriber networks. While seemingly simple, this setup will enable a comprehensive set of topics to be covered. Each network will initially be based on only its routing infrastructure, but from there we will continue to add additional layers of the networks, including switches, servers, and even desktops.

## Labs

The labs will follow the creation, configuration, and operation of Internet Service Provider 1 (ISP1), Internet Service Provider 2 (ISP2), Enterprise 1 (ENT1), and Enterprise 2 (ENT2).

Each ISP has a central office (CO), and will establish equipment shelters (ES) to house remote plant and equipment to extend the network. The two ISPs are independent, but will find value in peering with eachother to exchange regional traffic.

Each enterprise has a headquarters (HQ), and will establish branch offices (BO) to house staff and inventory across their market areas.  Eventually, the two enterprises will do business with eachother over the network by utilizing the two ISPs.

* Lab 1: Virtual environment setup
* Configure ISP1
* Configure ENT1 HQ
* Configure ENT1 HQ WAN with ISP1
* Configure ISP2
* Configure ENT1 HQ WAN with ISP2
* Configure ISP1 and ISP2 peering via BGP
* Configure ENT2 HQ
* Configure ENT2 HQ WAN with ISP2
